#include <iostream>
#include <vector>
#include <cassert>
#include <math.h>
#include <Eigen/Dense>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <fstream>
#include "BC.hpp"

auto addBC = [](Eigen::MatrixXd &tempMatrix, BC &MyBC_left, BC 
                    &MyBC_right, BC &MyBC_bottom, BC &MyBC_top){

    int rows = tempMatrix.rows();
    int cols = tempMatrix.cols();
        
        char mytype;
        double x;
        
        // Left Side
        std::cout << '\n' << '\n' << "set BC on the left side: If Dirichlet, type (d); if Neummann type (n)" << std::endl;
        std::cin >> mytype;

        if (mytype == 'd'){    
            std::cout << "Set the value of the Dirichlet BC:" << std::endl;
            std::cin >> x;
            MyBC_left.setval(x);
            MyBC_left.settype(BoundaryType::Dirichlet);

            // set BC on the left side
            for(size_t i = 0; i < rows; i++){
                tempMatrix(i,0) = MyBC_left.getval();
            }        
        }
        else if (mytype == 'n'){
            std::cout << "Set the value of the Neumann BC:" << std::endl;
            std::cin >> x;
            MyBC_left.setval(x);
            MyBC_left.settype(BoundaryType::Neumann);
        }
        else{
            std::cout << "Please type only 'd' or 'n'." << std::endl;
        }
        // Right Side
        std::cout << "set BC on the right side: If Dirichlet, type (d); if Neummann type (n)" << std::endl;
        std::cin >> mytype;

        if (mytype == 'd'){    
            std::cout << "Set the value of the Dirichlet BC:" << std::endl;
            std::cin >> x;
            MyBC_right.setval(x);
            MyBC_right.settype(BoundaryType::Dirichlet);

            // set BC on the right side
            for(size_t i = 0; i < rows; i++){
                tempMatrix(i,cols-1) = MyBC_right.getval();
            }        
        }
        else if (mytype == 'n'){
            std::cout << "Set the value of the Neumann BC:" << std::endl;
            std::cin >> x;
            MyBC_right.setval(x);
            MyBC_right.settype(BoundaryType::Neumann);
        }
        else{
            std::cout << "Please type only 'd' or 'n'." << std::endl;
        }
        
        //Bottom Side
        std::cout << "set BC on the botton side: If Dirichlet, type (d); if Neummann type (n)" << std::endl;
        std::cin >> mytype;

        if (mytype == 'd'){    
            std::cout << "Set the value of the Dirichlet BC:" << std::endl;
            std::cin >> x;
            MyBC_bottom.setval(x);
            MyBC_bottom.settype(BoundaryType::Dirichlet);

            // set BC on the botton side
            for(int j = 0; j < cols; j++){
                tempMatrix(rows-1, j) = MyBC_bottom.getval();
            }
        }
        else if (mytype == 'n'){
            std::cout << "Set the value of the Neumann BC:" << std::endl;
            std::cin >> x;
            MyBC_bottom.setval(x);
            MyBC_bottom.settype(BoundaryType::Neumann);
        }
        else{
            std::cout<< mytype << "Please type only 'd' or 'n':" << std::endl;
        }

        //Top Side
        std::cout << "set BC on the top side: If Dirichlet, type (d); if Neummann type (n)" << std::endl;
        std::cin >> mytype;

        if (mytype == 'd'){    
            std::cout << "Set the value of the Dirichlet BC:" << std::endl;
            std::cin >> x;
            MyBC_top.setval(x);
            MyBC_top.settype(BoundaryType::Dirichlet);

            // set BC on the top side
            for(int j = 0; j < cols; j++){
                tempMatrix(0, j) = MyBC_top.getval();
            }
        }
        else if (mytype == 'n'){
            std::cout << "Set the value of the Neumann BC:" << std::endl;
            std::cin >> x;
            MyBC_top.setval(x);
            MyBC_top.settype(BoundaryType::Neumann);
        }
        else{
            std::cout<< mytype << "Please type only 'd' or 'n':" << std::endl;
        }
};