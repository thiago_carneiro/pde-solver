// Finite Difference Scheme class
#ifndef __FINITEDIFFSCHEME_HPP_
#define __FINITEDIFFSCHEME_HPP_

class FiniteDiffScheme
{
    
    private:

    double _r;               // _r = hx/hy
    double _C, _N, _S, _E, _W;   //central, north, south, east and west points of the grid
    
    public:
    
    void set_C(double new_C){ _C = new_C;}
    void set_N(double new_N){ _N = new_N;}
    void set_S(double new_S){ _S = new_S;}
    void set_E(double new_E){ _E = new_E;}
    void set_W(double new_W){ _W = new_W;}

    double get_C(){return _C;}
    double get_N(){return _N;}
    double get_S(){return _S;}
    double get_E(){return _E;}
    double get_W(){return _W;}

    FiniteDiffScheme(double r){

        _C = -2-2*r*r;
        _N = 1.0;
        _S = 1.0;
        _E = 1.0*r*r;
        _W = 1.0*r*r;
    }

};

#endif