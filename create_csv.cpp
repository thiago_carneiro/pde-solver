#include <fstream>
#include<iostream>
#include<cassert>

auto creat_csv = [](std::string fileName, Eigen::MatrixXd tempMatrix)       //function to save reults in a CSV file
    {
        // https://eigen.tuxfamily.org/dox/structEigen_1_1IOFormat.html
        const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision, Eigen::DontAlignCols, ", ", "\n");
    
        std::ofstream file(fileName);
        if (file.is_open())
        {
            file << tempMatrix.format(CSVFormat);
            file.close();
        }
    };