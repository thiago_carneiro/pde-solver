# STEADY-STATE HEAT EQUATION SOLVER  

This project presents a 2D PDE-solver written in C++ for the steady-state heat equation $`T_{xx} + T_{yy} = 0`$, 
where $`T_{xx} =\frac{\partial ^{2} T}{\partial x^{2}}`$ and $`T_{yy} = \frac{\partial ^{2}T}{\partial y^{2}}`$. 
The solver is based on the Eigen library and uses a Finite Difference Method (FDM) scheme based on central differences. 

## Problem Definition

We consider a steady-state temperature distribution over a 2D plate.

Either Dirichlet (temperature values defined on the boundary) or Neumann (heat flux through the boundary) boundary conditions can be set as input by the user for the top, bottom, left and right boundaries.

The resolutions of the grid are $`Ny`$ and $`Nx`$, which are represented by the sketch below. $`h_{x} = \frac{1}{1+Nx}`$ and $`h_{y} = \frac{1}{1+Ny}`$

#### It is important to remark that to guarantee a unique solution, the user must set at least two Dirichlet B.C.

What is the temperature distribution on the plate?


```
Sketch of the domain

  __________________
 |                  |    ^
 |                  |    |
 |      plate       |    N_x
 |                  |    |
 |__________________|    v

 <------- N_y ------>
  
```


## Boundary Conditions

Boundary conditions used for heat conduction problems are the following:

* Temperature specified on the boundary (Dirichlet): $`T(x,t) = T_{e}(x,t)`$
   
* Heat flux across the boundary (Neumann): $`q \cdot n = q_{n}`$ or $`q \cdot n = 0`$
   
* Heat transfer to another medium with reference temperature $`T_{r}`$ and heat transfer coefficient $`\alpha`$ on the boundary: $`q \cdot n = \alpha *(T - T_{r})`$
    
* Thermal radiation to free space on the boundary: $`q \cdot n = q_{tr} = \rho *T^4`$

* This code works with both Dirichlet or Neumann B.C. on all four boundaries.


## Simulation using C++ and the Eigen library

The heat equation is solved using a Finite Difference Method scheme, which gives a linear system of equations (LSE) $`Ax = b`$ with $`(Nx \cdot Ny)`$ unknowns and $`(Nx \cdot Ny)`$ equations. $`A`$ is a $`(Nx \cdot Ny) \times (Nx \cdot Ny)`$ matrix, $`x`$ is a $`(Nx \cdot Ny)`$ vector with unknown temperature values, and $`b`$ is a $`(Nx \cdot Ny)`$ vector with known values. 

The LSE is solved using two types of solvers: 
* (1) LU decomposition with the Eigen library (recomended when $`Nx`$ and $`Ny`$ are really big).
* (2) Gauss-Seidel solver.

* For more information on the Eigen library and how to download it in your machine, access: https://eigen.tuxfamily.org/

## Compilation 

* On the terminal, compile the source-code using Cmake:

cmake CMakeLists.txt
$`\\`$
make 

* These two steps will generate the binary file 'heat', which can be run on the Terminal by the following command:

./heat

* In case the user does not have Cmake set, one can compile the source-code by running on the terminal the following step:

g++ -I ~/PATH-TO-EIGEN_LIBRARY heatEqSol2D.cpp -o name-output-file

* It will generate a binary output file called "name-output-file", which can be run using the following command:

./name_output_file 

## Instructions

The user must set the inputs:

* $`Nx`$ = Resolution for the $`x`$ dimension of the domain
* $`Ny`$ = Resolution for the $`y`$ dimension of the domain

* Boundary conditions for all sides:

(1) Choose Dirichlet (d) or Neumann (n) for the left boundary $`\rightarrow`$ set $`T_{left}`$ or $`q_{left}`$
$`\\`$
(2) Choose Dirichlet (d) or Neumann (n) for the right boundary $`\rightarrow`$ set $`T_{right}`$ or $`q_{right}`$
$`\\`$
(3) Choose Dirichlet (d) or Neumann (n) for the bottom boundary $`\rightarrow`$ set $`T_{bottom}`$ or $`q_{botton}`$
$`\\`$
(4) Choose Dirichlet (d) or Neumann (n) for the top boundary $`\rightarrow`$ set $`T_{top}`$ or $`q_{top}`$

* What kind of solver to use: Direct Solver or Gauss-Seidel



## Generating Surface Plot with Matplotlib

* The user has the option to generate a surface plot of the solution with Matplotlib. To do so, you must have the Developer version of Python3.8 installed on your system and also pandas (Python Data Analysis Library). For more information on how to download Python and its basic libraries, please access the following links: 

(1) https://www.python.org/
$`\\`$
(2) https://pandas.pydata.org/

* Once you have everything setup in your machine, uncomment the parts indicated in the file "heatEqSol2D.cpp" and run the following command on the terminal:

g++ -I ~/PATH-TO-EIGEN_LIBRARY heatEqSol2D.cpp -I /PATH-TO-python3.8/ -lpython3.8 -o name-output-file

* It will generate a binary output file called "name-output-file", which can be run using the following command:

./name_output_file 

As result, the solution for each temperature value on each node of the plate will be saved as a CSV file with the name "result.csv" and --in case the user choose to plot the surface-- a figure called "heat_eq.png" will be created with the surface plot. 
