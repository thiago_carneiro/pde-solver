#ifndef __SOLVER_DIRECT_HPP_
#define __SOLVER_DIRECT_HPP_

#include<iostream>
#include<vector>
#include<cassert>
#include<math.h>
#include<Eigen/Dense>
#include<stdio.h>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include<fstream>
#include "BC.hpp"
#include "FiniteDiffScheme.hpp"

void solver_direct(Eigen::MatrixXd &tempMatrix);
         
auto createSystemEquations (const Eigen::MatrixXd& tempMatrix, Eigen::MatrixXd& A, Eigen::VectorXd& b, BC &MyBC_left, BC 
                &MyBC_right, BC &MyBC_bottom, BC &MyBC_top, size_t Nx, size_t Ny, double c);

#endif
