cmake_minimum_required(VERSION 3.8)
project(HeatTransfer2D VERSION 0.2.0 LANGUAGES CXX)

find_package (Eigen3 3.3 REQUIRED NO_MODULE)

add_executable(heat heatEqSol2D.cpp)
target_link_libraries (heat Eigen3::Eigen)

target_compile_features(heat PUBLIC cxx_std_17)
set (CMAKE_CXX_FLAGS "-O0")