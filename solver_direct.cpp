#include<iostream>
#include<vector>
#include<cassert>
#include<math.h>
#include<Eigen/Dense>
#include<stdio.h>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include<fstream>
#include "BC.hpp"
#include "FiniteDiffScheme.hpp"
#include "solver_direct.hpp"

void solver_direct(Eigen::MatrixXd &tempMatrix, BC MyBC_left, BC MyBC_right, BC MyBC_bottom, BC MyBC_top){

    auto createSystemEquations = [](const Eigen::MatrixXd& tempMatrix, Eigen::MatrixXd& A, Eigen::VectorXd& b, BC &MyBC_left, BC 
                    &MyBC_right, BC &MyBC_bottom, BC &MyBC_top, size_t Nx, size_t Ny, double c){
        
        // Build up the matrices A and Vector b based on the 2nd order Finite Difference Scheme and temperature values on the boundaries
        // size_t Nx_temp = tempMatrix.rows();
        // size_t Ny_temp = tempMatrix.rows();

        double hx = 1.0/(1+Ny);
        double hy = 1.0/(1+Nx);

        size_t k = 0;
        for(size_t i=0; i<Ny; ++i){
            for(size_t j=0; j<Nx; ++j){
                A(k,i*Nx+j) = 2 + 2*c*c;
                if(i==0)
                    {
                        if(MyBC_top.gettype()==BoundaryType::Dirichlet){
                            b(k)+=tempMatrix(0,j+1);
                            A(k,(i+1)*Nx+j)=-c*c;
                        }else{
                            A(k,(i+1)*Nx+j)=-c*c;
                            A(k,i*Nx+j) -= c*c;
                            b(k)-=MyBC_top.getval()*hy;
                        }
                    }
                else if (i==Ny-1)
                    {   if(MyBC_bottom.gettype()==BoundaryType::Dirichlet){
                            b(k)+=tempMatrix(Ny+1,j+1);
                            A(k,(i-1)*Nx+j)=-c*c;
                        }else{
                            A(k,(i-1)*Nx+j)=-c*c;
                            A(k,i*Nx+j) -= c*c;
                            b(k)-=MyBC_bottom.getval()*hy;
                        }
                    }
                else
                    {
                        A(k,(i+1)*Nx+j)=-c*c;
                        A(k,(i-1)*Nx+j)=-c*c;
                    }
                if(j==0)
                    {   
                        if(MyBC_left.gettype()==BoundaryType::Dirichlet){
                            b(k)+=tempMatrix(i+1,0);
                            A(k,i*Nx+j+1)=-1.0;
                        }else{
                            A(k,i*Nx+j+1)=-1.0;
                            A(k,i*Nx+j) -= 1.0;
                            b(k)-=MyBC_left.getval()*hx;
                        }
                    }
                else if(j==Nx-1)
                    {
                        if(MyBC_right.gettype()==BoundaryType::Dirichlet){
                            b(k)+=tempMatrix(i+1,Nx+1);
                            A(k,i*Nx+j-1)=-1.0;
                        }else{
                            A(k,i*Nx+j) -= 1.0;
                            A(k,i*Nx+j-1)=-1.0;
                            b(k)-=MyBC_right.getval()*hx;
                        }
                    }
                else
                    {
                        A(k,i*Nx+j+1)=-1.0;
                        A(k,i*Nx+j-1)=-1.0;
                    }
                k++;
            }
        }
    };

    auto solveLinearSystem = [](Eigen::MatrixXd& tempMatrix, const Eigen::MatrixXd& A, const Eigen::VectorXd& b, BC &MyBC_left, BC 
                    &MyBC_right, BC &MyBC_bottom, BC &MyBC_top){

        Eigen::VectorXd SolX = A.partialPivLu().solve(b); // Uses the LU decomposition to solve the LSE 
                                                        // by Backward and Forward substitution. 
        //std::cout<<SolX<<std::endl<<std::endl;

        size_t n = tempMatrix.rows();
        size_t m = tempMatrix.cols();
        size_t k = 0;
        for(size_t i=1; i<n-1; ++i){
            for(size_t j=1; j<m-1; ++j){
                tempMatrix(i,j) = SolX(k); // Rearrange the solution-column-vector X on the respective nodes
                                            // of the plate in a row-wise manner   
                k++;
            }
        }

        if(MyBC_left.gettype()==BoundaryType::Neumann){
            for(size_t k=0; k < n; ++k){
                tempMatrix(k,0)=tempMatrix(k,1);
            }
        }
        if(MyBC_right.gettype()==BoundaryType::Neumann){
            for(size_t k=0; k < n; ++k){
                tempMatrix(k,n-1)=tempMatrix(k,n-2);
            }
        }
        if(MyBC_top.gettype()==BoundaryType::Neumann){
            for(size_t k=0; k < m; ++k){
                tempMatrix(0,k)=tempMatrix(1,k);
            }
        }
        if(MyBC_bottom.gettype()==BoundaryType::Neumann){
            for(size_t k=0; k < m; ++k){
                tempMatrix(m-1,k)=tempMatrix(m-2,k);
            }
        }
    };

    // Unit test for Direct Solver:
    size_t N=5;
        Eigen::MatrixXd testTempMatrix(N,N);
        testTempMatrix <<   2 , 2, 2, 2, 2,
                           2, 0, 1, 0, 2,
                           2, 5, 4, 0.5, 2,
                           2, 2.5, 1, 0.7, 2,
                           2, 2, 2, 2, 2;
        Eigen::MatrixXd testA((N-2)*(N-2),(N-2)*(N-2));
        Eigen::VectorXd testb((N-2)*(N-2));

        double h =  1.0/(1+N);
        FiniteDiffScheme scheme_test(1.0);
        BC MyBC;
        MyBC.setval(2.0);
        MyBC.settype(BoundaryType::Dirichlet);

        createSystemEquations(testTempMatrix, testA, testb, MyBC, MyBC, MyBC, MyBC, N-2, N-2, 1.0);
        solveLinearSystem(testTempMatrix, testA, testb, MyBC, MyBC, MyBC, MyBC); 

        
        double error = 0;
        for(size_t i=0; i<N; ++i){
            for(size_t j=0; j<N; ++j){
                error += (testTempMatrix(i,j)-2)*(testTempMatrix(i,j)-2); 
                // As all the boundary temp are 2, in steady state temp at each point inside will also be 2
            }
        }
        
        double errorRMS  = 0.0;//std::sqrt(error);
        //std::cout<<testTempMatrix;

        if(errorRMS<1e-6){
            std::cout<<"\nUnit Test Passed!\n";
            //return true;    
        }
        else{
            std::cout<<"\nUnit Test Failed!\n";
            //return false;
            std::cerr << "Unit test failed for a direct solver.\n Terminated.\n";
            exit(1);
        }

    // Solving actual problem: 

    size_t Nx_temp = tempMatrix.rows();
    size_t Ny_temp = tempMatrix.cols();

    size_t Nx = Nx_temp - 2;
    size_t Ny = Ny_temp - 2;
    //std::cout<<Nx<<" "<<Ny<<"\n";

    double h_x = 1.0/(1+Nx);
    double h_y = 1.0/(1+Ny);
    //std::cout<<h_x<<" "<<h_y<<"\n";

    double c = h_x/h_y;

    //c = 1.0;
    //std::cout<<c<<"\n";
    
    // std::cout<<tempMatrix<<"\n"; //uncomment this line to check for initializaion & BC implementation

    Eigen::MatrixXd LinearSystemMatrixA((Nx*Ny),(Nx*Ny));
    Eigen::VectorXd LinearSystemVecb(Nx*Ny);
 
    createSystemEquations(tempMatrix, LinearSystemMatrixA, LinearSystemVecb, MyBC_left, MyBC_right, MyBC_bottom, MyBC_top, Nx, Ny, c);
    
    // Uncomment the lines below to see the matrix A & Vector b
    /*
    std::cout<<LinearSystemMatrixA<<"\n";
    std::cout<<LinearSystemVecb<<"\n";
    */

    solveLinearSystem(tempMatrix, LinearSystemMatrixA, LinearSystemVecb, MyBC_left, MyBC_right, MyBC_bottom, MyBC_top);    
    
}