#ifndef __SOLVER_GAUSS_HPP_
#define __SOLVER_GAUSS_HPP_

#include<iostream>
#include<vector>
#include<cassert>
#include<math.h>
#include<Eigen/Dense>
#include<stdio.h>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include<fstream>
#include "BC.hpp"
#include "FiniteDiffScheme.hpp"


void solver_gauss(Eigen::MatrixXd &tempMatrix);

auto solverGauss(Eigen::MatrixXd &tempMatrix,
                        FiniteDiffScheme &scheme, BC &MyBC_left,
                        BC &MyBC_right, BC &MyBC_bottom, BC &MyBC_top,
                        double h_x, double h_y, double c);

                        
#endif
