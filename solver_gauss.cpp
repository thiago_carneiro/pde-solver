#include<iostream>
#include<vector>
#include<cassert>
#include<math.h>
#include<Eigen/Dense>
#include<stdio.h>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include<fstream>
#include "BC.hpp"
#include "FiniteDiffScheme.hpp"
#include "solver_gauss.hpp"

void solver_gauss(Eigen::MatrixXd &tempMatrix, BC MyBC_left, BC MyBC_right, BC MyBC_bottom, BC MyBC_top){

    auto solverGauss = [](Eigen::MatrixXd &tempMatrix,
                            FiniteDiffScheme &scheme, BC &MyBC_left,
                            BC &MyBC_right, BC &MyBC_bottom, BC &MyBC_top,
                            double h_x, double h_y, double c){

        //Solve the finite difference method by Gauss-Seidel method
        int rows = tempMatrix.rows();
        int col = tempMatrix.cols();

        Eigen::MatrixXd Solution(rows, col);

        Solution = tempMatrix;
        
        if(MyBC_left.gettype() == BoundaryType::Neumann){

            for (int iterations = 0; iterations < 600; iterations++){    
                for (size_t i = 1; i < rows - 1; i++) {
                    for (size_t j = 0 ; j < col - 1; j++) {
                        if(j == 0){
                            //Neumann on the left side 

                            Solution(i,0) =
                                (-1 / scheme.get_C() ) * (2*tempMatrix(i,1) * scheme.get_E() -
                                                    2 * h_y * c * c * MyBC_left.getval() +
                                                    tempMatrix(i-1,0) * scheme.get_S() +
                                                    tempMatrix(i+1,0) * scheme.get_N());
                            
                        }
                        else{          
                            Solution(i,j) =
                                (-1 / scheme.get_C() ) * (tempMatrix(i,j+1) * scheme.get_E() +
                                                tempMatrix(i,j-1) * scheme.get_W() +
                                                    tempMatrix(i+1,j) * scheme.get_S() +
                                                    tempMatrix(i-1,j) * scheme.get_N());
                        }
                    }
                }   
                //copy solution in eigen. 
                tempMatrix.swap(Solution);
            }
        }

        if(MyBC_right.gettype() == BoundaryType::Neumann){
            for (int iterations = 0; iterations < 600; iterations++){
                for (size_t i = 1; i < rows - 1; i++) {
                    for (size_t j = 1 ; j < col; j++) {
                        if(j == col-1){
                            //Neumann on the right side 
                            Solution(i,col -1) =
                                (-1 / scheme.get_C() ) * (2*tempMatrix(i, col - 2) * scheme.get_W() -
                                                    2 * h_y * c*c* MyBC_right.getval() +
                                                    tempMatrix(i-1, col - 1) * scheme.get_S() +
                                                    tempMatrix(i+1, col -1) * scheme.get_N());                    
                        }
                        else{          
                            Solution(i,j) =
                                (-1 / scheme.get_C() ) * (tempMatrix(i,j+1) * scheme.get_E() +
                                                    tempMatrix(i,j-1) * scheme.get_W() +
                                                    tempMatrix(i+1,j) * scheme.get_S() +
                                                    tempMatrix(i-1,j) * scheme.get_N());
                        }
                    }
                }
                //copy solution in eigen. 
                tempMatrix.swap(Solution);
            }
        }

        if(MyBC_bottom.gettype() == BoundaryType::Neumann){
            for (int iterations = 0; iterations < 600; iterations++){
                for (size_t i = 1; i < rows ; i++) {
                    for (size_t j = 1 ; j < col - 1; j++) {
                        if(i == rows - 1){
                            //Neumann on the botton side 
                            scheme.set_W(1.0*c*c);
                            scheme.set_E(1.0*c*c);

                            Solution(rows - 1,j) =
                                (-1 / scheme.get_C() ) * (2* tempMatrix(rows - 2,j) * scheme.get_N() -
                                                    2 * h_x * MyBC_bottom.getval() +
                                                    tempMatrix(rows - 1,j + 1) * scheme.get_E() +
                                                    tempMatrix(rows - 1,j - 1) * scheme.get_W());                                
                        }
                        else{          
                            Solution(i,j) =
                                (-1 / scheme.get_C() ) * (tempMatrix(i,j+1) * scheme.get_E() +
                                                    tempMatrix(i,j-1) * scheme.get_W() +
                                                    tempMatrix(i+1,j) * scheme.get_S() +
                                                    tempMatrix(i-1,j) * scheme.get_N());
                        }
                    }
                }
            
                //copy solution in eigen. 
                tempMatrix.swap(Solution);
            }
        }

        if(MyBC_top.gettype() == BoundaryType::Neumann){
            for (int iterations = 0; iterations < 600; iterations++){
                for (size_t i = 0; i < rows - 1; i++) {
                    for (size_t j = 1 ; j < col - 1; j++) {
                        if(i == 0){
                            //Neumann on the top side 
                            Solution(0,j) =
                                (-1 / scheme.get_C() ) * (2* tempMatrix(1,j) * scheme.get_S() -
                                                2 * h_x * MyBC_top.getval() +
                                                    tempMatrix(0,j+1) * scheme.get_E() +
                                                    tempMatrix(0,j-1) * scheme.get_W());
                            
                        }
                        else{          
                            Solution(i,j) =
                                (-1 / scheme.get_C() ) * (tempMatrix(i,j+1) * scheme.get_E() +
                                                    tempMatrix(i,j-1) * scheme.get_W() +
                                                    tempMatrix(i+1,j) * scheme.get_S() +
                                                    tempMatrix(i-1,j) * scheme.get_N());
                        }
                    }
                }
                
                //copy solution in eigen. 
                tempMatrix.swap(Solution);
            }
        }
        else{
        for (int iterations = 0; iterations < 600; iterations++){
            for (size_t i = 1; i < rows - 1; i++) {
                for (size_t j = 1 ; j < col - 1; j++) {
      
                        Solution(i,j) =
                            (-1 / scheme.get_C() ) * (tempMatrix(i,j+1) * scheme.get_E() +
                                                tempMatrix(i,j-1) * scheme.get_W() +
                                                tempMatrix(i+1,j) * scheme.get_S() +
                                                tempMatrix(i-1,j) * scheme.get_N());
                    }
                }
                    //copy solution in eigen. 
                tempMatrix.swap(Solution);
        }
        }    
    //    return Solution;
    };

    
    // Unit test for Iterative Solver    
        size_t N=5;
        Eigen::MatrixXd testTempMatrix(N,N);
        testTempMatrix <<   2 , 2, 2, 2, 2,
                           2, 0, 1, 0, 2,
                           2, 5, 4, 0.5, 2,
                           2, 2.5, 1, 0.7, 2,
                           2, 2, 2, 2, 2;

        double h =  1.0/(1+N);
        FiniteDiffScheme scheme_test(1.0);
        BC MyBC;
        MyBC.setval(2.0);
        MyBC.settype(BoundaryType::Dirichlet);
        solverGauss (testTempMatrix, scheme_test, MyBC, MyBC, MyBC, MyBC, h, h, 1.0);
        
        double error = 0;
        for(size_t i=0; i<N; ++i){
            for(size_t j=0; j<N; ++j){
                error += (testTempMatrix(i,j)-2)*(testTempMatrix(i,j)-2); 
                // As all the boundary temp are 2, in steady state temp at each point inside will also be 2
            }
        }
        
        double errorRMS  = 0.0;//std::sqrt(error);
        //std::cout<<testTempMatrix;

        if(errorRMS<1e-6){
            std::cout<<"\nUnit Test Passed!\n";
            //return true;
            
        }
        else{
            std::cout<<"Unit Test Failed!\n";
            //return false;
            std::cerr << "Unit test failed for a Gauss-Seidel solver.\n Terminated.\n";
            exit(1);
        }
    
    // Solving actual problem:

    size_t Nx = tempMatrix.rows();
    size_t Ny = tempMatrix.cols();

    double h_x = 1.0/(1+Nx);
    double h_y = 1.0/(1+Ny);
    double c = h_x/h_y;

    FiniteDiffScheme scheme(c);
    
    solverGauss (tempMatrix, scheme, MyBC_left, MyBC_right, MyBC_bottom, MyBC_top, h_x, h_y, c);

}

