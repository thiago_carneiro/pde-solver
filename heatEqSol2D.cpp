#include <iostream>
#include <vector>
#include <cassert>
#include <math.h>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <fstream>
#include <Eigen/Dense>
// #include <Python.h> // uncomment this to run the code with python
#include "BC.hpp"
#include "addBC.cpp"
#include "solver_gauss.cpp"
#include "solver_direct.cpp"
#include "create_csv.cpp"



int main(){

std::cout << "```" << std::endl
<< "Sketch of the domain" << std::endl
<<"    " << std::endl
<< "  __________________  " << std::endl
<< " |                  |    ^  " << std::endl
<< " |                  |    |  " << std::endl
<< " |      plate       |    N_x  " << std::endl
<< " |                  |    |  " << std::endl
<< " |__________________|    v  " << std::endl
<<"    " << std::endl
<<" <------- N_y ------>  " << std::endl
<<"    " << std::endl  
<< "```" << std::endl
<<"    " << std::endl;

    // set resolution (Nx = Ny = N = number of points)
    int Nx = 0;
    int Ny = 0;
    
    std::cout << "Please, set the resolution x  (Nx = positive_integer_number) of the grid" << std::endl;

    while(1){
        std::cin >> Nx;
        if(Nx <= 0){
            std::cout << "Please insert only a positive integer different from 0\n";
            std::cin.clear();
            std::cin.ignore(10, '\n');
            continue;
        }
        break;
    }


    std::cout << "Please, set the resolution y  (Ny = positive_integer_number) of the grid" << std::endl;

    while(1){
        std::cin >> Ny;
        if(Ny <= 0){
            std::cout << "Please insert only a positive integer different from 0\n";
            std::cin.clear();
            std::cin.ignore(10, '\n');
            continue;
        }
        break;
    }

    // set the grid:
    Eigen::MatrixXd tempMatrix(Nx + 2,Ny + 2);

    // Create BC objects
    BC MyBC_left;
    BC MyBC_right;
    BC MyBC_bottom;
    BC MyBC_top;

    addBC(tempMatrix, MyBC_left, MyBC_right, MyBC_bottom, MyBC_top);

    // Choose the solver 
    char choose_solver;
    char mychoice;

    while(1) {
        std::cout << '\n' << '\n' << "PLEASE, CHOOSE YOUR SOLVER:" << '\n';
        std::cout<< " To use Direct Solver, press (d). To use Gauss-Seidel solver, press (g):" << std::endl;
        std::cin >> choose_solver;

        if(choose_solver == 'd'){
            std::cin.clear();
	    std::cin.ignore(10, '\n');
	    solver_direct(tempMatrix, MyBC_left, MyBC_right, MyBC_bottom, MyBC_top);
            break;
        }
        else if(choose_solver =='g'){
            std::cin.clear();
            std::cin.ignore(10, '\n');
            solver_gauss(tempMatrix, MyBC_left, MyBC_right, MyBC_bottom, MyBC_top);
            break;
        }
        std::cout << "Please choose a solver correctly" << std::endl;
        std::cin.clear();
        std::cin.ignore(10, '\n');
    }  
    
    // std::cout<<tempMatrix<<std::endl;

    creat_csv("result.csv", tempMatrix);

	char filename[] = "quick_plotter.py";
	FILE* fp;

    // Uncomment bellow to run with python and generate the surface plot

    /*
	Py_Initialize();

	fp = _Py_fopen(filename, "r");
	PyRun_SimpleFile(fp, filename);

    Py_Finalize();
    */

    std::cout <<'\n'<<'\n'<< "Done! The result T(x, y) distribution was written in result.csv\n";
	return 0;

}