import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm



answer = pd.read_csv('result.csv', header=None)
Ny, Nx = np.shape(answer) #Ni here stands for Ni + 2 ofc



x = np.linspace(0, 1, Nx)
y = np.linspace(1, 0, Ny)
X, Y = np.meshgrid(x, y)
Z = answer


# Plot the surface.
fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

#plt.contourf(X, Y, answer, np.linspace(min(answer.min()), max(answer.max()), 200))
# plt.colorbar()
plt.title("T(x, y) distribution")
plt.savefig('heat_eq.png', dpi=300)
