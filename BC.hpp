//Boundary Condition Class
#ifndef __BC_HPP_
#define __BC_HPP_

enum class BoundaryType{
    Dirichlet,
    Neumann
};
 
class BC{

    private:
    BoundaryType _type;
    double _boundval;

    public:
    void setval(double boundval){
        _boundval = boundval;
    }
    double getval(){
        return _boundval;
    }
    void settype(BoundaryType type){
        _type = type;
    }
    
    BoundaryType gettype(){
        return _type;
    }

    BC(){
        _boundval = 0.0;
        _type = BoundaryType::Dirichlet;
    }
};


#endif